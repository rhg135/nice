/**
 * NICE : Nearly Intelligible Communications Entity
 * 
 * Eval: Stream and command handling.
 * 
 * TODO
 * 	Accept all IRC input from our newly (partially) integrated IRC handling.
 * 	Import all basic commands from Hab.
 */
#ifndef _NICE_EVAL_
#define _NICE_EVAL_

namespace nicebot {
	// Placeholder

} // namespace nicebot

#endif // _NICE_EVAL_
