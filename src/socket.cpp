/*
 * Copyright (C) 2013 Nathan Bass <https://github.com/IngCr3at1on>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * NICE : Nearly Intelligible Communications Entity
 * 
 * socket control
 * 
 * TODO
 * 	Add XMPP and JIM socket support.
 */

#include <iostream>
#include <signal.h>

#include "socket.h"

using namespace nicebot;

volatile bool running;

void signalHandler(int signal) {
	running = false;
}

// Get the current time (this is broken).
char *current() {
	time_t rawtime;
	struct tm *timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	return asctime(timeinfo);
}

void NiceSocket::InitSocket() {
	if(_irc.InitSocket()) {
		if(_irc.Connect(irc_host, irc_port)) {
			if(_irc.Login(irc_botnick, irc_realname)) {
				running = true;
				signal(SIGINT, signalHandler);

				while(_irc.Connected() && running)
					_irc.ReceiveData();
			}

			if(_irc.Connected())
				_irc.Disconnect();
	
			std::cout << "Disconnected : " << current << std::endl;
		}
	}
}
