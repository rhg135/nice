/**
 * NICE : Nearly Intelligible Communications Entity
 */

//#include "nice.h"
#include "socket.h"

using namespace nicebot;

void start() {
	NiceSocket _socket;
	
	_socket.InitSocket();
}

int main() {
	start();

	return 0;
}
