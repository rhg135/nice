/*
 * Copyright (C) 2013 Nathan Bass <https://github.com/IngCr3at1on>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * NICE : Nearly Intelligible Communications Entity
 * 
 * socket control
 * 
 * TODO
 * 	Add XMPP and JIM socket support.
 * 	Add default channel for IRC into here (somehow).
 */
#ifndef _NICE_SOCKET_
#define _NICE_SOCKET_

#include "../lib/irc/IRCEval.h"

namespace nicebot {
	class NiceSocket {
		public:
			NiceSocket() {
				irc_botnick = "NICEntity";
				irc_realname = "NICE, a simple FOSS bot";
				irc_host = "irc.freenode.org";

				irc_port = 6667;
			}

			void InitSocket();

		private:
			IRCEval _irc;

			const char *irc_botnick;
			const char *irc_realname;
			const char *irc_host;

			int irc_port;

	}; // class NiceSocket
} // namespace nicebot

#endif // _NICE_SOCKET_
