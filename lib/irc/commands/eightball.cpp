#include <cstdlib>
#include <string>
#include <vector>

#include "eightball.h"

std::string get_responses(std::vector<std::string> vec) {
	int rind = rand() % vec.size();
	return vec[rind];
}

std::string get_response() {
	std::vector<std::string> responses;
	responses.push_back("It is certain.");
	responses.push_back("Unlikely");
	responses.push_back("It is uncertain");
	responses.push_back("Signs point to yes.");
	responses.push_back("Try again later.");
	return get_responses(responses);
}
