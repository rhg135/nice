/*
 * Copyright (C) 2013 Nathan Bass <https://github.com/IngCr3at1on>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * NICE : Nearly Intelligible Communications Entity
 * 
 * Store commands here for now until we think of a better way to handle add-ons.
 */

#ifndef _IRCCOMMANDS_H
#define _IRCCOMMANDS_H

#include "eightball.h"
#include "roulette.h"

#endif // _IRCCOMMANDS_H
