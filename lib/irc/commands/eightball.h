#ifndef _EIGHTBALL_H
#define _EIGHTBALL_H

//Functionally pure version
std::string get_response(std::vector<std::string>);

//Inpure handy function
std::string get_response();

#endif
