
#ifndef _ROULETTE_H

#define _ROULETTE_H

#include <vector>
#include <string>

enum bullet {
	BLANK,
	NORMAL,
	KILL
}

std::vector<enum Bullet> pistol;

std::vector<enum Bullet> get_pistol(int size);

bool load_pistol(string nick, string data);

bool pop_pistol(string nick, string data);

#endif
