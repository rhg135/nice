/*
 * Copyright (C) 2013 Nathan Bass <https://github.com/IngCr3at1on>
 * Copyright (C) 2011 Fredi Machado <https://github.com/Fredi>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * NICE : Nearly Intelligible Communications Entity
 * 
 * IRC command evaluation partially based on:
 * 		https://github.com/Fredi/IRCClient
 * 
 * TODO
 * 	Rewrite to pipe information to eval.
 */

#ifndef _IRCEVAL_H
#define _IRCEVAL_H

#include <list>
#include <string>
#include <vector>

#include "IRCSocket.h"

#include "../utils/utils.h"

namespace nicebot {
	struct IRCCommandPrefix {
		void Parse(std::string data) {
			if(data == "")
				return;

			prefix = data.substr(1, data.find(" ") - 1);
			std::vector<std::string> tokens;

			if(prefix.find("@") != std::string::npos) {
				tokens = split(prefix, '@');
				nick = tokens.at(0);
				host = tokens.at(1);
			}
			if(nick != "" && nick.find("!") != std::string::npos) {
				tokens = split(nick, '!');
				nick = tokens.at(0);
				user = tokens.at(1);
			}
		};

		std::string prefix;
		std::string nick;
		std::string user;
		std::string host;
	};

	struct IRCMessage {
		IRCMessage();
		IRCMessage(std::string cmd, IRCCommandPrefix p, std::vector<std::string> params) :
			command(cmd), prefix(p), parameters(params) {};

		std::string command;
		IRCCommandPrefix prefix;
		std::vector<std::string> parameters;
	};

	class IRCEval {
		public:
			IRCEval() {
				debug = false;
				_postmotd = false;
			}

			bool InitSocket() { return _socket.Init(); }

			bool Connect(const char* host, int port) {
				return _socket.Connect(host, port);
			}
			void Disconnect() { _socket.Disconnect(); }
			bool Connected() { return _socket.Connected(); }

			bool SendIRC(std::string data) {
				data.append("\n");
				std::cout << data << std::endl;
				return _socket.SendData(data.c_str());
			}

			/* SendIRC, JoinChan and PartChan do not require a nick variable
			 * but I'm overloading them to allow allow message handling to be
			 * sorted in structs. */
			bool SendIRC(std::string nick, std::string data) {
				return SendIRC(data);
			}

			bool SendMSG(std::string dest, std::string content) {
				SendIRC("PRIVMSG " + dest + " :" + content);
			}

			bool JoinChan(std::string chan) {
				/* If we didn't get a '376' message from the server than there's
				 * no autojoin, an admin can still force nice to join a channel
				 * by private message. If this is done we need to set this to
				 * true to activate other command evaluation. */
				if(_postmotd == false) _postmotd = true;
				SendIRC("JOIN " + chan);
			}

			bool JoinChan(std::string nick, std::string chan) {
				return JoinChan(chan);
			}
			
			bool PartChan(std::string chan) {
				SendIRC("PART " + chan);
			}

			bool PartChan(std::string nick, std::string chan) {
				return PartChan(chan);
			}

			bool Login(std::string nick, std::string real) {
				_nick = nick;
				_real = real;

				if(SendIRC("NICK " + nick))
					if(SendIRC("USER " + nick + " 8 * :" + real))
						return true;

				return false;
			}

			void ReceiveData();

			void Parse(std::string /*data*/);

			// Default internal handlers
			void HandlePrivMsg(IRCMessage /*message*/);
			void HandleNotice(IRCMessage /*message*/);
			void HandleChannelJoinPart(IRCMessage /*message*/);
			void HandleUserNickChange(IRCMessage /*message*/);
			void HandleUserQuit(IRCMessage /*message*/);
			void HandleChannelNamesList(IRCMessage /*message*/);
			void HandleNicknameInUse(IRCMessage /*message*/);
			void HandleServerMessage(IRCMessage /*message*/);

			bool Consult8Ball(std::string, std::string);
			bool LoadPistol(std::string, std::string);

			/* While no status check is needed for these we want them to be
			 * accessible from the ircMsgTable. */
			bool help(std::string /*nick*/);
			bool help(std::string /*nick*/, std::string /*command*/);
			// We also don't need nick but the ircMsgHandling requires it.
			bool MessageNick(std::string /*origin*/, std::string /*data*/);;

		private:
			IRCSocket _socket;

			std::string _nick;
			std::string _real;

			bool debug;
			/* track if the MOTD has completed because there's no reason to
			 * evaluate it. The bot crashes if trying to evaluate the full MOTD
			 * with the current multi-argument command handling for text
			 * commands. This is a cheap fix as flooding the bot is still
			 * possible. */
			bool _postmotd;
	}; // class IRCEval
} // namespace nicebot

#endif // _IRCEVAL_H
