/*
 * Copyright (C) 2013 Nathan Bass <https://github.com/IngCr3at1on>
 * Copyright (C) 2011 Fredi Machado <https://github.com/Fredi>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * NICE : Nearly Intelligible Communications Entity
 * 
 * IRC command evaluation partially based on:
 * 		https://github.com/Fredi/IRCClient
 * 
 * TODO
 * 	Rewrite to pipe information to eval.
 * 	Place message commands in a struct or map.
 */

#include <algorithm>
#include <iostream>
#include <sstream>

#include "IRCEval.h"
#include "commands/IRCCommands.h"

using namespace nicebot;

#define NUM_IRC_CMDS 26
#define NUM_IRC_MSGS 7

std::string admin = "IngCr3at1on";

struct IRCCommandHandler {
	std::string command;
	void (IRCEval::*handler)(IRCMessage /*message*/);
};

IRCCommandHandler ircCommandTable[NUM_IRC_CMDS] = {
	{ "PRIVMSG",            &IRCEval::HandlePrivMsg                   },
	{ "NOTICE",             &IRCEval::HandleNotice                    },
	{ "JOIN",               &IRCEval::HandleChannelJoinPart           },
	{ "PART",               &IRCEval::HandleChannelJoinPart           },
	{ "NICK",               &IRCEval::HandleUserNickChange            },
	{ "QUIT",               &IRCEval::HandleUserQuit                  },
	{ "353",                &IRCEval::HandleChannelNamesList          },
	{ "433",                &IRCEval::HandleNicknameInUse             },
	{ "001",                &IRCEval::HandleServerMessage             },
	{ "002",                &IRCEval::HandleServerMessage             },
	{ "003",                &IRCEval::HandleServerMessage             },
	{ "004",                &IRCEval::HandleServerMessage             },
	{ "005",                &IRCEval::HandleServerMessage             },
	{ "250",                &IRCEval::HandleServerMessage             },
	{ "251",                &IRCEval::HandleServerMessage             },
	{ "252",                &IRCEval::HandleServerMessage             },
	{ "253",                &IRCEval::HandleServerMessage             },
	{ "254",                &IRCEval::HandleServerMessage             },
	{ "255",                &IRCEval::HandleServerMessage             },
	{ "265",                &IRCEval::HandleServerMessage             },
	{ "266",                &IRCEval::HandleServerMessage             },
	{ "366",                &IRCEval::HandleServerMessage             },
	{ "372",                &IRCEval::HandleServerMessage             },
	{ "375",                &IRCEval::HandleServerMessage             },
	{ "376",                &IRCEval::HandleServerMessage             },
	{ "439",                &IRCEval::HandleServerMessage             },
};

inline int const GetCommandHandler(std::string command) {
	for(int i = 0; i < NUM_IRC_CMDS; ++i) {
		if(ircCommandTable[i].command == command)
			return i;
	}
	return NUM_IRC_CMDS;
}

struct IRCMsgHandler {
	std::string handle;
	std::string help_msg;
	std::string admin_help_msg;
	bool (IRCEval::*handler)(std::string /*nick*/, std::string /*data*/);
	std::string data;
	bool private_reply_only;
	bool admin_cmd;
	int cmd_args;
};

IRCMsgHandler ircMsgTable[NUM_IRC_MSGS] = {
	{	"8ball",
		"Consults the magic 8-ball.",
		"Consults the magic 8-ball.",
		&IRCEval::Consult8Ball,
		"",
		false,
		false,
		1
	},
	{	"rload"
		"Loads the pistol"
		"Loads the pistol"
		&IRCEval::LoadPistol,
		"",
		false,
		false,
		-1
	},
	{	"cli",
		"Sends a link to a handy CLI cheatsheet.",
		"Sends a link to a handy CLI cheatsheet.",
		&IRCEval::SendMSG,
		"http://terokarvinen.com/command_line.html",
		false,
		false,
		0 // Specify as a non-argumental command.
	},
	{	"help",
		"cli, source", // seen
		"join, part", // deop, kick, me, msg, op, opme
		&IRCEval::help,
		"", // Used to redirect to main help instead of cmd help.
		true, // we want help to be sent as a private message so's not to spam.
		false, // this is not an admin command
		-1 // Support argumental and non-argumental functionality.
	},
	{	"join",
		"This command is for admins only.",
		"Join a channel, usage: join <channel>",
		&IRCEval::JoinChan,
		"", // not needed
		false,
		true,
		1 // join requires 1 argument.
	},
	{	"msg",
		"Message command not enabled yet.",
		"Message command not enabled yet.",
		&IRCEval::MessageNick,
		"", // not needed
		false,
		false,
		2 // message accepts 2 arguments
	},
	{	"part",
		"This command is for admins only.",
		"Part a channel, usage: part <channel>",
		&IRCEval::PartChan,
		"", // not needed
		false,
		true,
		1
	},
	{	"source",
		"Provides a link to the bot source code.",
		"Provides a link to the bot source code.",
		&IRCEval::SendMSG,
		"https://bitbucket.org/IngCr3at1on/nice",
		false,
		false,
		0
	},
};

inline int const GetIrcMsgHandler(std::string handle) {
	for(int i = 0; i < NUM_IRC_MSGS; ++i) {
		if(ircMsgTable[i].handle == handle)
			return i;
	}
	return NUM_IRC_MSGS;
}

void IRCEval::ReceiveData() {
	std::string buffer = _socket.ReceiveData();
	std::string line;
	std::istringstream iss(buffer);
	while(getline(iss, line)) {
		if(line.find("\r") != std::string::npos)
			line = line.substr(0, line.size() - 1);

		Parse(line);
	}
}

void IRCEval::Parse(std::string data) {
	std::string original(data);

	if(debug == true) {
		std::cout << "debug: " << original << std::endl;
	}

	IRCCommandPrefix cmdPrefix;

	// if command has prefix
	if(data.substr(0, 1) == ":") {
		cmdPrefix.Parse(data);
		data = data.substr(data.find(" ") + 1);
	}

	std::string command = data.substr(0, data.find(" "));
	std::transform(command.begin(), command.end(), command.begin(), towupper);
	if(data.find(" ") != std::string::npos) {
		data = data.substr(data.find(" ") + 1);
	} else {
		data = "";
	}

	std::vector<std::string> parameters;
	
	if(data != "") {
		if(data.substr(0, 1) == ":") {
			parameters.push_back(data.substr(1));
		} else {
			size_t pos1 = 0, pos2;
			while ((pos2 = data.find(" ", pos1)) != std::string::npos) {
				parameters.push_back(data.substr(pos1, pos2 - pos1));
				pos1 = pos2 + 1;
				if(data.substr(pos1, 1) == ":") {
					parameters.push_back(data.substr(pos1 + 1));
					break;
				}
			}
			if(parameters.empty()) {
				parameters.push_back(data);
			}
		}
	}

	if(command == "ERROR") {
		if(debug != true) {
			std::cout << original << std::endl;
		}
		Disconnect();
		return;
	}

	if(command == "PING") {
		SendIRC("PONG :" + parameters.at(0));
		return;
	}

	IRCMessage ircMessage(command, cmdPrefix, parameters);

	// Default handler
	int commandIndex = GetCommandHandler(command);
	if(commandIndex < NUM_IRC_CMDS) {
		IRCCommandHandler& cmdHandler = ircCommandTable[commandIndex];
		(this->*cmdHandler.handler)(ircMessage);
		if(debug != true) {
			std::cout << original << std::endl;
		}
	}
}

bool IRCEval::Consult8Ball(std::string nick, std::string data) {
	// I don't use data, but c++ cant express that
	SendMSG(nick, get_response());
	return true;
}

bool LoadPistol(std::string nick, std::string data) {
	
bool IRCEval::help(std::string nick) {
	SendMSG(nick, "Currently supported commands are as follows:");

	/* Load the command hanlder for help so we have an easy reference to it's
	 * messages. */
	int commandIndex = GetIrcMsgHandler("help");
	IRCMsgHandler& cmdHandler = ircMsgTable[commandIndex];

	if(nick.compare(admin) == 0)
		SendMSG(nick, cmdHandler.admin_help_msg);

	SendMSG(nick, cmdHandler.help_msg);

	// This is never false, there's no reason to check it.
	return true;
}

bool IRCEval::help(std::string nick, std::string command) {
	if(command.compare("") == 0)
		return help(nick);

	int commandIndex = GetIrcMsgHandler(command);
	if(commandIndex < NUM_IRC_MSGS) {
		IRCMsgHandler& cmdHandler = ircMsgTable[commandIndex];
		if(nick.compare(admin) == 0) {
			SendMSG(nick, cmdHandler.admin_help_msg);
		} else {
			SendMSG(nick, cmdHandler.help_msg);
		}
	}
	return true;
}

void IRCEval::HandlePrivMsg(IRCMessage message) {
	/* Do not evaluate any private messages (other then admin ones) prior to
	 * receiving the MOTD */
	if(_postmotd != true && message.prefix.nick.compare(admin) != 0) {
		return;
	}

	std::string origin = message.parameters.at(0);
	std::string command = message.parameters.at(message.parameters.size() - 1);

	/* If a command is prefixed w/ 'n.' remove it for evaluation, but if the
	 * message is from a channel and not prefixed ignore it entirely (responds
	 * to non-prefixed private messages). */
	if(command[0] == 'n' && command[1] == '.') {
		command.erase(0,2);
	} else if(origin[0] == '#') {
		return;
	}

	if(command.compare("quit") == 0 && message.prefix.nick.compare(admin) == 0) {
		SendIRC("QUIT :Reloading, hopefully...");
		Disconnect();
		return;
	}

	// Evaluate non-argument commands.
	int commandIndex = GetIrcMsgHandler(command);
	if(commandIndex < NUM_IRC_MSGS) {
		IRCMsgHandler& cmdHandler = ircMsgTable[commandIndex];

		/* If this is a multi-arg command than it shouldn't have matched and the
		 * syntax is wrong, send the help messages. */
		if(cmdHandler.cmd_args > 0) {
			if(cmdHandler.admin_cmd == true) {
				SendMSG(message.prefix.nick, cmdHandler.admin_help_msg);
			} else if(origin[0] == '#') {
				SendMSG(origin, cmdHandler.help_msg);
			} else {
				SendMSG(message.prefix.nick, cmdHandler.help_msg);
			}
			return;
		}

		// If this is an admin command and the sender is not an admin, notify.
		if(cmdHandler.admin_cmd == true && message.prefix.nick.compare(admin) != 0) {
			if(origin[0] == '#') {
				SendMSG(origin, cmdHandler.help_msg);
			} else {
				SendMSG(message.prefix.nick, cmdHandler.help_msg);
			}
		} else {
			/* Respond to the origin channel but only if private_reply_only is not
			 * set. All other messages should direct to the senders nick (allows for
			 * things like help to be forced to sender instead of the channel). */
			if(cmdHandler.private_reply_only == false && origin[0] == '#') {
				(this->*cmdHandler.handler)(origin, cmdHandler.data);
			} else {
				(this->*cmdHandler.handler)(message.prefix.nick, cmdHandler.data);
			}
		}
		return;
	}

	// Evaluate all argumental commands
	for(int i = 0; i < NUM_IRC_MSGS; ++i) {
		IRCMsgHandler& cmdHandler = ircMsgTable[i];
		// Skip non-argumental commands as they have already been processed.
		if(cmdHandler.cmd_args == 0)
			continue;

		/* This doesn't quite work right unfortunately as it seems to match on
		 * more then just the first word. */
		std::string handle = cmdHandler.handle;
		if(command.find(handle) != std::string::npos) {
			if(cmdHandler.admin_cmd == true && message.prefix.nick.compare(admin)  != 0) {
				if(origin[0] == '#') {
					SendMSG(origin, cmdHandler.help_msg);
				} else {
					SendMSG(message.prefix.nick, cmdHandler.help_msg);
				}
			} else {
				/* Use our cmd_args to check syntax. Leave a work-around for
				 * help cause we have it setup to accept single or multiple
				 * arguments. */
				std::vector<std::string> tokens = split(command, ' ');
				int args;
				/* Used a negative number to allow non-argumental and argumental
				 * commands but otherwise help is the only command which uses
				 * this and it takes 1 argument. */
				if(cmdHandler.cmd_args == -1) {
					args = 1;
				} else {
					args = cmdHandler.cmd_args;
				}
				// arguments plus the initial command.
				if(tokens.size() < args + 1) {
					if(cmdHandler.admin_cmd == true) {
						SendMSG(message.prefix.nick, cmdHandler.admin_help_msg);
					} else if(origin[0] == '#') {
						SendMSG(origin, cmdHandler.help_msg);
					} else {
						SendMSG(message.prefix.nick, cmdHandler.help_msg);
					}
					return;
				}

				// Remove the initial command handle.
				command.erase(0, handle.length()+1);
				/* Feed the origin/nick and command data through the given
				 * command handler. */
				if(cmdHandler.private_reply_only == false && origin[0] == '#') {
					(this->*cmdHandler.handler)(origin, command);
				} else {
					(this->*cmdHandler.handler)(message.prefix.nick, command);
				}
			}
			return;
		}
	}
}

void IRCEval::HandleNotice(IRCMessage message) {
	std::string from = message.prefix.nick != "" ? message.prefix.nick : message.prefix.prefix;
	std::string text = message.parameters.at(message.parameters.size() - 1);

	std::cout << "-" << from << "- " << text << std::endl;
}

void IRCEval::HandleChannelJoinPart(IRCMessage message) {
	std::string channel = message.parameters.at(0);
	std::string action = message.command == "JOIN" ? "joins" : "leaves";

	std::cout << message.prefix.nick << " " << action << " " << channel << std::endl;
}

void IRCEval::HandleUserNickChange(IRCMessage message) {
	std::string newNick = message.parameters.at(0);

	std::cout << message.prefix.nick << " changed his nick to " << newNick << std::endl;
}

void IRCEval::HandleUserQuit(IRCMessage message) {
	std::string text = message.parameters.at(0);

	std::cout << message.prefix.nick << " quits (" << text << ")" << std::endl;
}

void IRCEval::HandleChannelNamesList(IRCMessage message) {
	std::string channel = message.parameters.at(2);
	std::string nicks = message.parameters.at(3);

	std::cout << "People on " << channel << ":" << std::endl << nicks << std::endl;
}
	
void IRCEval::HandleNicknameInUse(IRCMessage message) {
	std::cout << message.parameters.at(1) << " " << message.parameters.at(2) << std::endl;
}

void IRCEval::HandleServerMessage(IRCMessage message) {
	// Use MOTD message to trigger autojoin.
	if(message.command.compare("376") == 0) {
		_postmotd = true;
		JoinChan("#projectopencannibal");
	}
	if(message.command.compare("433") || message.command.compare("437")) {
		//regainnick();
	}

	std::vector<std::string>::const_iterator itr = message.parameters.begin();
	++itr; // skip the first parameter (our nick)
	for(; itr != message.parameters.end(); ++itr) {
		std::cout << *itr << " ";
	}
	std::cout << std::endl;
}

bool IRCEval::MessageNick(std::string origin, std::string data) {
	// Get the first word from our message (the destination channel/nick).
	std::string dest;
	if(data.find_first_of(' ') != std::string::npos) {
		dest = data.substr(0, data.find_first_of(' '));
	}

	// Clip the first word (the nick/channel off the front).
	clip(data);

	// Finally assemble and send the message.
	SendMSG(dest, data);
}
