/*
 * Copyright (C) 2013 Nathan Bass <https://github.com/IngCr3at1on>
 * Copyright (C) 2011 Fredi Machado <https://github.com/Fredi>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * NICE : Nearly Intelligible Communications Entity
 * 
 * IRCSocket: IRC stream handling, modified from the following:
 * 		https://github.com/Fredi/IRCClient
 * 
 * TODO
 * 	Allow for connecting to multiple servers and multiple channels
 * 		(should load from a separate header).
 */

#ifndef _IRCSOCKET_H
#define _IRCSOCKET_H

namespace nicebot {
	class IRCSocket {
		public:
			IRCSocket() {}

			bool Init();

			bool Connect(char const* /*host*/, int /*port*/);

			void Disconnect();

			bool Connected() { return _connected; };

			bool SendData(char const* /*data*/);

			std::string ReceiveData();

		private:
			int _socket;

			bool _connected;

	}; // class IRCSocket
} // namespace nicebot

#endif // _IRCSOCKET_H
