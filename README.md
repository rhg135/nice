# NICE
### Nearly Intelligible Communications Entity

A fairly basic C++ chat bot; currently running on IRC with hopes of adding XMPP
and JIM socket support.

---

Requirements:

* Linux only currently.

* CMake compatible C++ compiler (tested w/ gcc 4.8).

* CMake (tested w/ 2.8).

After installing cmake run the following to create a build/out directory and
perform an out-of-source build.

	mkdir build
	cd build
	cmake ../
	make
